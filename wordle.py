GREEN, YELLOW, RED = "G", "Y", "R"
SPACE = " "
WORDS = "5chars.txt"
GAME_URL = "https://we6.talentsprint.com"
REGISTER_URL = GAME_URL + "/game/register"
CREATE_URL = GAME_URL + "/game/create"
FEEDBACK_URL = GAME_URL + "/game/guess"
JSON_HEADER = {"Content-Type": "application/json"}

import requests as rq
import json

def reduce_possibles(possibles: list[str], trial: str, feedback: str) -> list[str]:
    possibles = possibles[1:]
    if GREEN in feedback:
        possibles = [possible for possible in possibles if match_greens(trial, feedback, possible)]
    if RED in feedback:
        possibles = [possible for possible in possibles if match_reds(trial, feedback, possible)]
    if YELLOW in feedback:
        possibles = [possible for possible in possibles if match_yellows(trial, feedback, possible)]
            
    return possibles


def green_positions(feedback: str) -> list[int]:
    return [i for i, ch in enumerate(feedback) if ch == GREEN]


def match_greens(trial: str, feedback: str, possible: str) -> bool:
    return all([trial[p] == possible[p] for p in green_positions(feedback)])


def match_reds(trial: str, feedback: str, possible: str) -> bool:
    reds = ''.join(a for (a, b) in zip(trial, feedback) if b == RED)
    return all(red not in possible for red in reds)


def match_yellows(trial: str, feedback: str, possible: str) -> bool:
    yellows = ''.join(a for (a, b) in zip(trial, feedback) if b == YELLOW)
    return all(yellow in possible for yellow in yellows)


def register() -> tuple[str, str]:
    reg = rq.post(REGISTER_URL, headers=JSON_HEADER, data='{"name": "asokan"}')
    # TODO: check r status
    biscuit = reg.cookies
    load = json.dumps({"id": reg.json()["id"], "overwrite": True})
    r = rq.post(CREATE_URL, headers=JSON_HEADER, data=load, cookies=biscuit)
    # TODO: check r status     
    return reg.json()["id"], r.cookies


def get_score(trial: str, me: str, biscuit: str) -> str:
    load = json.dumps({"id": me, "guess": trial})
    r = rq.post(FEEDBACK_URL, headers=JSON_HEADER, data=load, cookies=biscuit)    
    return r.json()["feedback"], r.cookies
        
possibles = [line.strip() for line in open(WORDS)]
me, biscuit = register()
trial = possibles[0]
trials = []
feedback, biscuit = get_score(trial, me, biscuit)
while feedback != "G" * len(trial):
    trials.append(trial)
    before = len(possibles)
    possibles = reduce_possibles(possibles, trial, feedback)
    after = len(possibles)
    print(f"Reduced from {before} to {after} with {trial}")
    trial = possibles[0]
    feedback, biscuit = get_score(trial, me, biscuit)
    if not feedback:
        print("Failed to figure out") 
        print(possibles)    
        break
else:
    print(f"Won with {trial}")
