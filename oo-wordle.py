SOURCE = "/Users/shifu.village/WE6/wordle/5chars.txt"

GAME_URL = "https://we6.talentsprint.com"
REGISTER_URL = GAME_URL + "/game/register"
CREATE_URL = GAME_URL + "/game/create"
FEEDBACK_URL = GAME_URL + "/game/guess"
JSON_HEADER = {"Content-Type": "application/json"}
FEEDBACK = "feedback"

GREEN, YELLOW, RED = "G", "Y", "R"
SPACE = " "
GAME_WON = "GGGGG"
READY = "READY"
LOST = "LOST!"

import requests as rq
import json

class WordleSolver:
    wordlist = []
    FIRST_TIME = True

    def __init__(self, who: str):
        if WordleSolver.FIRST_TIME:
            WordleSolver.FIRST_TIME = False
            WordleSolver.wordlist = [line.strip() for line in open(SOURCE)]

        self.possibles = WordleSolver.wordlist[:]
        self.player = who

                           
    def register_player(self):    
        load = json.dumps({"name": self.player})
        reg = rq.post(REGISTER_URL, headers=JSON_HEADER, data=load)
        if reg.status_code != rq.codes.created:
            raise RuntimeError(f"Something went wrong with registering, check \n {reg.content}")
        self.id = reg.json()["id"]
        self.biscuit = reg.cookies

    
    def create_game(self):
        load = json.dumps({"id": self.id, "overwrite": True})
        r = rq.post(CREATE_URL, headers=JSON_HEADER, data=load, cookies=self.biscuit)
        self.biscuit = r.cookies            
        self.status = READY
    

    def __str__(self):
        return str(self.id)   


    def guess(self):
        self.choose_word()
        load = json.dumps({"id": self.id, "guess": self.trial})
        r = rq.post(FEEDBACK_URL, headers=JSON_HEADER, data=load, cookies=self.biscuit)    
        if r.status_code == rq.codes.unprocessable:
            self.result = "LOST!"
            self.status = "OVER"
        elif r.status_code == rq.codes.ok:
            self.feedback = r.json()[FEEDBACK] 
            if self.feedback == GAME_WON:
                self.result = "WON!!"
                self.status = "OVER"
        self.biscuit = r.cookies            


    def choose_word(self):
        self.trial = self.possibles[0]
        self.possibles = self.possibles[1:]


    def update(self):
        def green_positions(feedback: str) -> list[int]:
            return [i for i, ch in enumerate(feedback) if ch == GREEN]

        def match_greens(trial: str, feedback: str, possible: str) -> bool:
            return all([trial[p] == possible[p] for p in green_positions(feedback)])

        def match_reds(trial: str, feedback: str, possible: str) -> bool:
            reds = ''.join(a for (a, b) in zip(trial, feedback) if b == RED)
            return all(red not in possible for red in reds)

        def match_yellows(trial: str, feedback: str, possible: str) -> bool:
            yellows = ''.join(a for (a, b) in zip(trial, feedback) if b == YELLOW)
            return all(yellow in possible for yellow in yellows)

        if GREEN in self.feedback:
            self.possibles = [_ for _ in self.possibles if match_greens(self.trial, self.feedback, _)]
        if RED in self.feedback:
            self.possibles = [_ for _ in self.possibles if match_reds(self.trial, self.feedback, _)]
        if YELLOW in self.feedback:
            self.possibles = [_ for _ in self.possibles if match_yellows(self.trial, self.feedback, _)]
            

    def solve(self):
        self.register_player()
        self.create_game()
        while self.status == READY:
            before = len(self.possibles)
            self.guess()
            self.update()
            print(f"Reduced from {before} to {len(self.possibles)} with {self.trial}")
        else:
            if self.result == LOST:
                print(self.possibles)
            else:
                print(self.trial)

w = WordleSolver("Aruvi")
w.solve()


# ----------------------------
# --- Add a check for even()
# ----------------------------
